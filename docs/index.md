---
author: ericECmorlaix
title: 🏡 Accueil
---

## **Toto est dans la place avec [PyGgb = Geogebra + Python](https://geogebra.org/python/index.html?name=toto&code=eJy1kctqwzAQRff6CkEXsUFOx3ZMvcmqtOtCl6WExkzaoWNLSE6%2BP4qDyQMrNoFsbIEu96BzN1bXsqUaJdVG21Y6RjRCdL8I5kUs3pjJOIw%2BNDVtlKRQPC9ApfnhK8mtduRozbh8%2F2GHsZKXucVLKJdDd8PU4Kr9o%2Bq%2FQefkUqaeec7%2FxN8afeOx15O73oHGPpDkocQQTMlKs7b%2BPFvzFmfXwCTr2vx7bmLHUiPoJ4CNR4e9T9T%2BIOu9hHKShGBqRILZWsM3Fpg4wd0blABQwmmGNBavZCvuNwA1WJuBB2anoi%2BfmxcKvkOaxR7h1wvx){:target="_blank" } !**

<figure>
<iframe
  id="toto-pyggb"
  title="Toto avec PyGgb"
  width="1000"
  height="800"
  src="https://geogebra.org/python/index.html?name=toto&code=eJy1kctqwzAQRff6CkEXsUFOx3ZMvcmqtOtCl6WExkzaoWNLSE6%2BP4qDyQMrNoFsbIEu96BzN1bXsqUaJdVG21Y6RjRCdL8I5kUs3pjJOIw%2BNDVtlKRQPC9ApfnhK8mtduRozbh8%2F2GHsZKXucVLKJdDd8PU4Kr9o%2Bq%2FQefkUqaeec7%2FxN8afeOx15O73oHGPpDkocQQTMlKs7b%2BPFvzFmfXwCTr2vx7bmLHUiPoJ4CNR4e9T9T%2BIOu9hHKShGBqRILZWsM3Fpg4wd0blABQwmmGNBavZCvuNwA1WJuBB2anoi%2BfmxcKvkOaxR7h1wvx">
</iframe>
</figure>

### Ressources pour [PyGgb = Geogebra + Python](https://geogebra.org/python/index.html){:target="_blank" } :

- <https://tice-c2i.apps.math.cnrs.fr/2023/10/21/pyggb/>{:target="_blank" }
- <http://revue.sesamath.net/spip.php?article1601>{:target="_blank" }
- <http://revue.sesamath.net/spip.php?article1620>{:target="_blank" }
- <https://mathematiques.ac-normandie.fr/Combiner-GeoGebra-et-Python>{:target="_blank" }
- <https://www.reddit.com/r/pyggb/>{:target="_blank" }
- <https://github.com/othoni-hub/PythonGgb>{:target="_blank" }



!!! info "Adapter ce site modèle"

    Le tutoriel est ici : [Tutoriel de site avec python](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
    
    Si vous voulez conserver certaines pages de ce modèle sans qu'elles ne soient visibles dans le menu, il suffit de les enlever du fichier `.pages`   
    Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite
    






